RailsDataTable::Application.routes.draw do


  #match '/backbone', :to => "users#backbone"
  root :to => 'user#backbone'
  
  resources :users do
    collection do
      get 'backbone', to: "users#backbone"
  end
end

end

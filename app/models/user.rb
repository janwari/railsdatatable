class User < ActiveRecord::Base
  attr_accessible :address, :age, :county, :gender, :name, :primary_language, :secondary_language, :telephone

  def as_json(options={})
    super(:except => [ :created_at, :updated_at ])
  end

end

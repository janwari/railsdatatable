
// A view to enable easy CRUD functionality using DataTables
(function (Backbone) {
  'use strict';
  var Events, generateErrorHTML, renderError;

  Events = Backbone.Events;

  Backbone.DataTableView = Backbone.View.extend({
    template: _.template('<div class="errors"></div>' +
            '<div class="modal-form"></div>' +
            '<button class="btn add-row pull-right span" data-toggle="modal">' +
            '<i class="icon-plus-sign"></i> Add</button>' +
            '<table class="table table-striped table-bordered"></table>'),

    editButtonHTML: '<button class="btn btn-mini edit-row">' +
             '<i class="icon-pencil"></i> Edit</button>',

    deleteButtonHTML: '<button class="btn btn-danger btn-mini delete-row">' +
             '<i class="icon-trash icon-white"></i> Delete</button>',

    /**
     * Doing our initialization here
     * Caching reference to current view's table in this.$table
     */
    preRenderInitialize: function () {
      _.bindAll(this, 'render', 'renderDataTable', 'renderEditDeleteButton', 'handleError');
      Events.on('create' + this.cid, this.createRow, this);
      Events.on('update' + this.cid, this.updateRow, this);
      Events.on('table-drawn', this.alignEditDeleteButton, this);
      this.$el.html(this.template());
      this.$table = this.$('table');
    },

    events: {
      'click .add-row': 'addRow',
      'click .edit-row': 'editRow',
      'click .delete-row': 'deleteRow'
    },


    // Default DataTables settings.
    // See http://datatables.net/blog/Twitter_Bootstrap_2
    crudDataTableOptions: {
      'sDom': '<"row"<"span4"l><"pull-right"f>r>t<"row-fluid"<"span5"i><"span7"p>>',
      'sPaginationType': 'bootstrap',
      'aaSorting': [],
      'bDestroy': true,
      'oLanguage': {'sLengthMenu': '_MENU_ records per page'},
      'fnDrawCallback': function() {
          Events.trigger('table-drawn');
       }
    },


    render: function () {
      if (!this.dataTableOptions.aoColumnDefs || this.dataTableOptions.aoColumnDefs.length === 0){
        throw new Error('"aoColumnDefs" not found. Please define DataTables aoColumnDefs');
      }
      this.preRenderInitialize();
      this.collection.fetch({
        success: this.renderDataTable,
        error: this.handleError
      });
      
      window.jobj = this.dataTableOptions;
      console.log(this.dataTableOptions)
      return this;
    },

    renderDataTable: function (collection, response, options) {
      this.mergeDataTableOptions(response);
      this.$table.dataTable(this.crudDataTableOptions);
    },


    /**
     * Create a new model and display the modal form to allow users to input values
     * @param {Events} event "Add" button click
     */
    addRow: function (event) {
      var model = new this.collection.model();
      console.log(model)
      this.createAndRenderFormView(model);
    },


    /**
     * Add the saved model to collection
     * Calls DataTables fnAddData to add new model to the table
     * @param {Backbone.Model} model
     */
    createRow: function (model) {
      this.collection.add(model);
      this.$table.fnAddData(model.toJSON());
    },


    /**
     * Gets the model for corresponding row and displays the modal form for editing
     *
     * @param {Events} event "Edit" button click
     */
    editRow: function (event) {
      var id, model;
      id = this.getModelId($(event.currentTarget));
      model = this.collection.get(id);
      this.createAndRenderFormView(model);
    },


    /**
     * Updates the current row being edited with updated model values.
     * Finds the row to update by getting the position of the model in collection
     * Delegates to DataTables#fnUpdate to update the row
     *
     * @param {Backbone.Model} model
     */
    updateRow: function (model) {
      var rowIndex = this.collection.indexOf(model);
      this.$table.fnUpdate(model.toJSON(), rowIndex, undefined, false, false);
    },


    /**
     * Destroys a model and remove the corresponding row from the table
     *
     * @param {Event} event jQuery event object
     */
     deleteRow: function (event) {
      var self, id, model, continueDelete, rowIndex;

      continueDelete = _.isFunction(this.beforeDelete) ? this.beforeDelete() : true;
      if (continueDelete) {
        id = this.getModelId($(event.currentTarget));
        model = this.collection.get(id);
        rowIndex = this.collection.indexOf(model);
        self = this;
        model.destroy({
            success: function (model, response) {
              self.$table.fnDeleteRow(rowIndex);
            },
            error: this.handleError
          },{ wait: true });
      }
    },


    alignEditDeleteButton: function () {
      this.$table.find('td.edit-delete').css('text-align', 'center').css('min-width', '50px');
    },

    createAndRenderFormView: function (model) {
      var view = this.createFormView(model);
      this.displayModalForm(view);
    },

    /**
     * Create a Backbone.DataTableFormView and passes all user-defined methods
     * The current table's view.cid is also passed along to allow TableFormView
     * that is used when adding custom events
     *
     * @param {type} model
     * @returns {@exp;Backbone@call;DataTableFormView}
     */
    createFormView: function (model) {
      return new Backbone.DataTableFormView({
                model: model,
                template: this.formTemplate,
                tableViewCid: this.cid,
                serialize: this.serialize,
                compiledTemplate: this.compileTemplate,
                renderError: this.renderError
              });
    },
    /**
     * Add the form view (Bootstrap's Modal) to DOM
     *
     * @param {type} model
     * @returns {undefined}
     */
    displayModalForm: function (view) {
      this.$('div.modal-form').html(view.render().el);
      this.$('div.modal').modal('show');
    },

    handleError: function (collection, response, options) {
      renderError(response.responseText, this);
    },

    renderEditDeleteButton: function () {
      return this.editButtonHTML + '\u0009' + this.deleteButtonHTML;
    },

    /**
     *  Retrieve the model id for the current row where the edit/delete button was clicked
     *
     * @param {object} $target jQuery button object
     * @returns {int|string} id Model id
     */
    getModelId: function ($target) {
      var row, id;
      row = $target.closest('tr')[0];
      id = this.$table.fnGetData(row).id;
      return id;
    },

    /**
     * Merge the default DataTables settings with the one provided by the user
     * Additionally if "hideEditDeleteColumn" is not set then add column definition
     * for Edit/Delete column
     *
     * @param {type} response
     * @returns {undefined}
     */
    mergeDataTableOptions: function (response) {
      _.extend(this.crudDataTableOptions, this.dataTableOptions, {aaData: response});

      if (!this.hideEditDeleteColumn) {
        this.crudDataTableOptions.aoColumnDefs.push({
          'mData': null,
          'aTargets': [this.crudDataTableOptions.aoColumnDefs.length],
          'bSearchable': false,
          'bSortable': false,
          'sClass': 'edit-delete',
          'mRender': this.renderEditDeleteButton
        });
      }
      
    },

    /**
     * Unbinding element events
     * Unbind all collection events
     * Destroy DataTable generated table
     * Remove the view from the DOM
     */
    close: function () {
      this.off();
      this.collection.off(null, null, this);
      if (this.$table && $.fn.DataTable.fnIsDataTable(this.$table[0])) {
        this.$table.fnDestroy(true);
      }
      this.remove();
    }

  });


  /**
   * A modal view that displays the form for a model.
   *
   */
  Backbone.DataTableFormView = Backbone.View.extend({
    initialize: function () {
      _.bindAll(this, 'handleError');
      this.listenTo(this.model, 'invalid', this.handleError);
      this.listenTo(this.model, 'sync', this.close);
            console.log(this.options);

    },

    events: {
      'click .save-form': 'save',
      'click .close-form': 'close'
    },

    render: function () {
      this.template = this.compileTemplate(this.options.template);
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },

    /**
     * Compiles the template. By default uses Underscore.js "_.template" method.
     * User can override the default implementation by defining "compileTemplate"
     * in the Backbone.DataTableView
     *
     * @param {string} formTemplate
     * @returns {func}
     */
    compileTemplate: function(formTemplate){
      if (!formTemplate || formTemplate.length === 0){
        throw new Error('Form template not found. Please define a template');
      }
      var compiledTemplate;
      if (this.options.compileTemplate) {
        compiledTemplate = this.options.compileTemplate(formTemplate);
      }
      else if (_.isFunction(formTemplate)) {
        compiledTemplate = formTemplate;
      }
      else {
        compiledTemplate = _.template(formTemplate);
      }
      return compiledTemplate;
    },

    /**
     * Save model on server/persistence layer
     *
     *  On "success" triggers "update" event that will update the model's row in table
     *  On "error" calls handleError method to parse and display the errors
     *
     * @param {Event} event "Save" button click
     */
    save: function (event) {
      var eventName = this.model.isNew() ? 'create' : 'update';
      eventName += this.options.tableViewCid;

      this.model.save(this.serialize(), {
        success: function (model, response, options) {
          Events.trigger(eventName, model);
        },
        error: this.handleError
      });
    },

    /**
     * Serializes the form input values to an object
     * By default uses Ben Alman's jQuery serializeObject plugin
     * Users can override the default serialization by
     * defining their own serialize method
     *
     * @returns {object} attrs
     */
    serialize: function () {
      var attrs = {};
      if (this.options.serialize) {
        attrs = this.options.serialize.call(this);
      }
      else {
        attrs = this.$('form').serializeObject();
      }
      return attrs;
    },

    handleError: function (model, error) {
      if (_.isObject(error) && error.responseText) {
        error = error.responseText;
      }
      renderError(error, this);
    },

    close: function () {
      this.$('div.modal').modal('hide');
      if (!_.isEmpty(this.model.validationError)) {
        this.model.attributes = this.model.previousAttributes();
      }
      this.remove();
      this.unbind();
    }
  });


  //
  // Common helper methods used by Backbone.DataTableFormView and Backbone.DataTableView
  //

  /**
   * Get the HTML for the errors and adds it to the view, most often <div class="errors">
   * Users can override the default error handling by providing their own
   * renderError method.
   *
   * Note: this method is shared by both DataTableView and DataTableFormView
   *
   * @param {array|string} error
   * @param {Backbone.View} view  Backbone.DataTableView/Backbone.DataTableFormView
   */
  renderError = function (errors, view) {
    if (view.renderError) {
      view.renderError(errors);
    }
    else if (view.options.renderError) {
      view.options.renderError(errors);
    }
    else {
      var errorsHTML = generateErrorHTML(errors);
      view.$el.find('div.errors').html(errorsHTML);
    }
  };

 /**
   * Generate the HTML necessary for displaying the error
   * Depending on the type of the error argument the generated HTML could be
   *    A UL consisting a list of all errors
   *    A P tag containing the error
   *
   * @param {array|string} error
   * @returns {String} errorHTML
   */
  generateErrorHTML = function (error) {
    var errorHTML;

    if (_.isArray(error) && error.length > 1) {
      errorHTML = '<div class="alert alert-error"><ul>';
      _.each(error, function (errorItem) {
        errorHTML += '<li>' + errorItem + '</li>';
      });
      errorHTML += '</ul></div>';
    }
    else {
      errorHTML = '<p class="alert alert-error">' + error + '</p>';
    }
    return errorHTML;
  };

}(Backbone));

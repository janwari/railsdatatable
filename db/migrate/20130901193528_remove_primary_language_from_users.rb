class RemovePrimaryLanguageFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :primary_language
  end

  def down
    add_column :users, :primary_language, :string
  end
end

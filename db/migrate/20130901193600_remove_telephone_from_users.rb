class RemoveTelephoneFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :telephone
  end

  def down
    add_column :users, :telephone, :string
  end
end

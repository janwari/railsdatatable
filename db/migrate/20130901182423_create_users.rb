class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.integer :age
      t.string :gender
      t.string :county
      t.string :primary_language
      t.string :secondary_language
      t.string :telephone
      t.string :address

      t.timestamps
    end
  end
end

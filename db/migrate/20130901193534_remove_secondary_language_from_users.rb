class RemoveSecondaryLanguageFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :secondary_language
  end

  def down
    add_column :users, :secondary_language, :string
  end
end

class RemoveCountyFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :county
  end

  def down
    add_column :users, :county, :string
  end
end
